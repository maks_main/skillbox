FROM node

RUN mkdir /app
workdir /app
COPY package.json /app
   
run yarn install
copy . /app
run yarn test
run yarn build

cmd yarn start
EXPOSE 3000 
